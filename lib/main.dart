import 'package:flutter/material.dart';
import 'package:wisata_bandung/main_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Wisata Bandung',
      theme: ThemeData.dark(),
        color: Colors.black,
        home: MainScreen(),
    );
  }
}


