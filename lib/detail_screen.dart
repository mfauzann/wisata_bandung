import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wisata_bandung/model/tourism_place.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/services.dart';


class DetailScreen extends StatelessWidget {
  final TourismPlace place;
  DateTime currentBackPressTime;
  var informationTextStyle = TextStyle(
    fontFamily: 'Oxygen',
  );
  DetailScreen({@required this.place});

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Perhatian, Anda akan keluar dari Aplikasi", gravity: ToastGravity.CENTER,);
      return Future.value(false);
    }
    return SystemNavigator.pop();
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: onWillPop,
      child: new Scaffold(
          backgroundColor: Colors.black,
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Stack(
                    children: <Widget>[
                      Image.network(place.imageAsset),
                      Row(
                        children: <Widget>[
                          Container(
                            child: SafeArea(
                              child: IconButton(icon: Icon(Icons.arrow_back,color: Colors.black,),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  }),
                            ),
                          ),
                          Container(
                            alignment: Alignment.topRight,
                            child: SafeArea(
                              left: false,
                              right: true,
                              child:  FavoriteButton(),
                            ),
                          ),
                        ],
                      ),
                    ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 16.0),
                  child: Text(
                    place.name,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 30.0,
                      fontFamily: 'Staatliches',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Icon(Icons.calendar_today),
                          SizedBox(height: 8.0),
                          Text(place.openDays, style: informationTextStyle,),
                        ],
                      ),
                      Column(
                          children: <Widget>[
                            Icon(Icons.access_time),
                            SizedBox(height: 8.0),
                            Text(place.openTime,style: informationTextStyle,),
                          ]
                      ),
                      Column(
                        children: <Widget>[
                          Icon(Icons.monetization_on),
                          SizedBox(height: 8.0),
                          Text(place.ticketPrice,style: informationTextStyle,),
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                      place.description,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.0,fontFamily: 'Oxygen',),
                  ),
                ),
                Container(
                  height: 150,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: place.imageUrls.map((url){
                      return Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(url),
                         ),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
          ),
      ),
    );
  }
}

class FavoriteButton extends StatefulWidget {

  @override
  _FavoriteButtonState createState() => _FavoriteButtonState();
}

class _FavoriteButtonState extends State<FavoriteButton> {
  bool isFavorite = false;
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        isFavorite ? Icons.favorite : Icons.favorite_border,
        color: Colors.red,
      ),
      onPressed: () {
        setState(() {
          isFavorite = !isFavorite;
        });
      },
    );
  }
}
